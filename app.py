from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import xml.etree.cElementTree as ET
import datetime
import json
import geoip2.database

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1BJd_if3M9yrw39joVpcQFMTn6ZQnxlnMDKs6toLLuZU'
SAMPLE_RANGE_NAME = 'FOREO!A2:P'

def get_reviews(write_json= False):
    """

    :param write_json: write results in json format to "reviews.json" file. For debug purpose.
    :return: multidimensional dictionary grouped by product SKU
    """
    creds = None
    reviews = {}
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        i = 1
        for row in values:
            if row[4] not in reviews:
                reviews[row[4]] = [(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15])]
            else:
                reviews[row[4]].append((row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15]))
            i += 1
            if write_json:
                j = json.dumps(reviews)
                f = open("reviews.json", "w")
                f.write(j)
                f.close()
    return reviews


def create_xml(dictionary):
    """ Creates xml document based on BV specification
    https://knowledge.bazaarvoice.com/wp-content/conversations/en_US/Collect/native_content_import.html#full-feed-examples

    :param dictionary: reviews in dict format grouped by product SKU
    """
    feed = ET.Element("Feed", xmlns="http://www.bazaarvoice.com/xs/PRR/StandardClientFeed/14.7", name="Foreo", extractDate=datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))
    review_id = 1
    for sku, review in dictionary.items():
        product = ET.SubElement(feed, "Product", id=str(sku))
        external_id = ET.SubElement(product, "ExternalId").text = str(sku)
        rs = ET.SubElement(product, "Reviews")
        for item in review:
            # print(type(item))
            # print(item)
            # break
            r = ET.SubElement(rs, "Review", id=str(review_id), removed="false")
            mc = ET.SubElement(r, "ModerationStatus").text = "APPROVED"

            # UserProfileReference start
            upr = ET.SubElement(r, "UserProfileReference", id=str(item[3]))
            external_id = ET.SubElement(upr, "ExternalId").text = item[3]
            display_name = ET.SubElement(upr, "DisplayName").text = item[3]
            anonymous = ET.SubElement(upr, "Anonymous").text = "false"
            hle = ET.SubElement(upr, "HyperlinkingEnabled").text = "false"
            # UserProfileReference ends

            ra1 = ET.SubElement(r, "Title").text = item[11]
            ra2 = ET.SubElement(r, "ReviewText").text = item[12]
            ra3 = ET.SubElement(r, "Rating").text = item[13]
            ra4 = ET.SubElement(r, "IpAddress").text = item[2]
            ra5 = ET.SubElement(r, "Recommended").text = "true"
            ra6 = ET.SubElement(r, "UserEmailAddress").text = item[14]
            # ra7 = ET.SubElement(r, "ReviewerLocation").text = "Austin Texas"
            ra8 = ET.SubElement(r, "SubmissionTime").text = format_date(item[1])
            ra9 = ET.SubElement(r, "Featured").text = "false"
            ra10 = ET.SubElement(r, "DisplayLocale").text = get_locale(item[15], item[2])

            review_id += 1

    tree = ET.ElementTree(feed)
    tree.write("reviews.xml", encoding="utf-8", xml_declaration=True)


def format_date(date_string):
    dt = datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S')
    return dt.strftime('%Y-%m-%dT%H:%M:%S')


def get_locale(language, ip):
    """Get locale string with <language_COUNTRY> format_date

    :param language: two-digit language code
    :param ip: ip address
    :return: Standard locale string
    """
    reader = geoip2.database.Reader('GeoIP2-Country.mmdb')
    response = reader.country(ip)
    return language.lower() + '_' + response.country.iso_code


if __name__ == '__main__':
    reviews = get_reviews()
    create_xml(reviews)
